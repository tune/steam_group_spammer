#include "steam.hpp"

// construct and load our own instance of steam_api.dll
Steam::Steam() : m_steam_api{LoadLibraryA(STR("steam_api.dll"))} {

    // open steam_appid.txt (required for steam_api to connect with a linked "game")
    m_appid.open(STR("steam_appid.txt"), std::fstream::out | std::fstream::binary | std::fstream::trunc);
    if (!m_appid.good()) {
        errprint();
        m_appid.close();
        return;
    }

    // append 1.6 appid (10) and weird 0xA to file then close
    m_appid << "10\xA";
    m_appid.close();

    if (m_steam_api == INVALID_HANDLE_VALUE) {
        errprint();
        return;
    }

    // retrieve initialize function exported by steam_api that handles connection to steam and other prereqs.
    if (!get_export<SteamAPI_Methods::Init_>(STR("SteamAPI_Init"), m_init)) 
        return;

    // invoke init and hope it doesn't return false
    if (!m_init()) {
        errprint();
        return;
    }
    
    // grab our createinterface export
    if (!get_export<SteamInternal_Methods::CreateInterface_>(STR("SteamInternal_CreateInterface"), m_create_interface))
        return;

    // grab pipe and user 'get' exports
    if (!get_export<SteamAPI_Methods::GetHSteamPipe_>(STR("SteamAPI_GetHSteamPipe"), m_get_steam_pipe))
        return;
    if (!get_export<SteamAPI_Methods::GetHSteamUser_>(STR("SteamAPI_GetHSteamUser"), m_get_steam_user))
        return;

    m_user = m_get_steam_user();
    m_pipe = m_get_steam_pipe();

    if (!m_user || !m_pipe) {
        errprint();
        return;
    }

    dbgprint("user %d\n", m_user);
    dbgprint("pipe %d\n", m_pipe);

    m_engine = create_interface<IClientEngine*>("CLIENTENGINE_INTERFACE_VERSION004");
    m_friends = m_engine->GetIClientFriends(m_user, m_pipe, "CLIENTFRIENDS_INTERFACE_VERSION001");
}

Steam::~Steam() {
    FreeLibrary(m_steam_api);
}
