#include "steam.hpp"

// credits:
// https://github.com/SteamRE/open-steamworks/
// Valve

int main(int argc, char** argv) {
    std::vector<CSteamID> friends, clans;

    Steam steam;

    auto friends_amt = steam.m_friends->GetFriendCount(EFriendFlags::k_EFriendFlagImmediate);
    dbgprint("total friends %d\n", friends_amt);

    auto clans_amt = steam.m_friends->GetClanCount();
    dbgprint("you're currently in %d clans\n", clans_amt);

    
    for (int c{}; c < clans_amt; ++c) {
        auto id = steam.m_friends->GetClanByIndex(c);
        if (!steam.m_friends->IsClanPublic(id))
            continue;
        clans.push_back(id);
    }
    
	int gay_manual_counter = 0;
    for (int i{}; i < friends_amt; ++i) {
        auto id = steam.m_friends->GetFriendByIndex(i, EFriendFlags::k_EFriendFlagImmediate);
        
        auto state = steam.m_friends->GetFriendPersonaState(id);
        if (state == EPersonaState::k_EPersonaStateOffline)
            continue;

        friends.push_back(id);
        dbgprint("%i: %s %s\n", gay_manual_counter++, steam.m_friends->GetFriendPersonaName(id), id.Render());
    }
   
    io::w_printf("target (#): ");
   
    char buf[64]{};
    std::cin.get(buf, 64);
   
    int target = atoi(buf);
    io::w_printf("raping %s\n", steam.m_friends->GetFriendPersonaName(friends[target]));
   
    for (auto& cid : clans){
        steam.m_friends->InviteFriendToClan(friends[target], cid);
    }
   
    system("pause");
    return std::cin.get();
}