#pragma once

#include <m_maddie.h>

#ifdef _DEBUG
#define dbgprint(str,  ...) io::w_printf("[dbg] "); io::w_printf(str, ##__VA_ARGS__)
#define errprint() io::w_printf("%s failed on line %d with error %d\n", __FUNCTION__, __LINE__, GetLastError())
#else
#define dbgprint(str,  ...) io::w_printf("[dbg] "); io::w_printf(str, ##__VA_ARGS__)
#define errprint() io::w_printf("%s failed on line %d with error %d\n", __FUNCTION__, __LINE__, GetLastError())
#endif