#pragma once

#include "inc.hpp"

// typedefs
using HSteamUser = int32_t;
using HSteamPipe = int32_t;


// enumerations
enum EUniverse
{
    k_EUniverseInvalid = 0,
    k_EUniversePublic = 1,
    k_EUniverseBeta = 2,
    k_EUniverseInternal = 3,
    k_EUniverseDev = 4,
    //k_EUniverseRC = 5, // Removed

    k_EUniverseMax
};

enum EAccountType
{
    k_EAccountTypeInvalid = 0,
    k_EAccountTypeIndividual = 1,		// single user account
    k_EAccountTypeMultiseat = 2,		// multiseat (e.g. cybercafe) account
    k_EAccountTypeGameServer = 3,		// game server account
    k_EAccountTypeAnonGameServer = 4,	// anonymous game server account
    k_EAccountTypePending = 5,			// pending
    k_EAccountTypeContentServer = 6,	// content server
    k_EAccountTypeClan = 7,
    k_EAccountTypeChat = 8,
    k_EAccountTypeConsoleUser = 9,		// Fake SteamID for local PSN account on PS3 or Live account on 360, etc.
    k_EAccountTypeAnonUser = 10,

    // Max of 16 items in this field
    k_EAccountTypeMax
};

enum EPersonaState
{
    k_EPersonaStateOffline = 0,			// friend is not currently logged on
    k_EPersonaStateOnline = 1,			// friend is logged on
    k_EPersonaStateBusy = 2,			// user is on, but busy
    k_EPersonaStateAway = 3,			// auto-away feature
    k_EPersonaStateSnooze = 4,			// auto-away for a long time
    k_EPersonaStateLookingToTrade = 5,	// Online, trading
    k_EPersonaStateLookingToPlay = 6,	// Online, wanting to play
    k_EPersonaStateMax,
};

enum EFriendFlags
{
    k_EFriendFlagNone = 0x00,
    k_EFriendFlagBlocked = 0x01,
    k_EFriendFlagFriendshipRequested = 0x02,
    k_EFriendFlagImmediate = 0x04,			// "regular" friend
    k_EFriendFlagClanMember = 0x08,
    k_EFriendFlagOnGameServer = 0x10,
    // k_EFriendFlagHasPlayedWith	= 0x20,	// not currently used
    // k_EFriendFlagFriendOfFriend	= 0x40, // not currently used
    k_EFriendFlagRequestingFriendship = 0x80,
    k_EFriendFlagRequestingInfo = 0x100,
    k_EFriendFlagIgnored = 0x200,
    k_EFriendFlagIgnoredFriend = 0x400,
    k_EFriendFlagSuggested = 0x800,
    k_EFriendFlagAll = 0xFFFF,
};

// function definitions
namespace SteamInternal_Methods {
    using CreateInterface_ = uintptr_t(*)(const char*);
}

namespace SteamAPI_Methods {
    using Init_ = bool(*)();

    using GetHSteamPipe_ = HSteamPipe(*)();
    using GetHSteamUser_ = HSteamUser(*)();
}

// various needed structures
class CSteamID {
protected:
    // 64 bits total
    union SteamID_t {
        struct SteamIDComponent_t {
            uint32_t			m_unAccountID : 32;			// unique account identifier
            unsigned int		m_unAccountInstance : 20;	// dynamic instance ID
            unsigned int		m_EAccountType : 4;			// type of account - can't show as EAccountType, due to signed / unsigned difference
            EUniverse			m_EUniverse : 8;	// universe this account belongs to
        } m_comp;

        uint64_t m_unAll64Bits;
    } m_steamid;

    
public:
    CSteamID() {
        m_steamid.m_comp.m_unAccountID = 0;
        m_steamid.m_comp.m_EAccountType = k_EAccountTypeInvalid;
        m_steamid.m_comp.m_EUniverse = k_EUniverseInvalid;
        m_steamid.m_comp.m_unAccountInstance = 0;
    }

    CSteamID(uint64_t _id) {
        m_steamid.m_unAll64Bits = _id;
    }

    uint64_t GetRawData() const {
        return m_steamid.m_unAll64Bits;
    }

    uint64_t ConvertToUint64() const
    {
        return m_steamid.m_unAll64Bits;
    }

    const char * Render() const				// renders this steam ID to string
    {
        const int k_cBufLen = 30;
        const int k_cBufs = 4;
        char* pchBuf;

        static char rgchBuf[k_cBufs][k_cBufLen];
        static int nBuf = 0;

        pchBuf = rgchBuf[nBuf++];
        nBuf %= k_cBufs;

        switch (m_steamid.m_comp.m_EAccountType)
        {
        case k_EAccountTypeInvalid:
        case k_EAccountTypeIndividual:
            sprintf(pchBuf, "STEAM_0:%u:%u", (m_steamid.m_comp.m_unAccountID % 2) ? 1 : 0, (uint32_t) m_steamid.m_comp.m_unAccountID / 2);
            break;
        default:
            sprintf(pchBuf, "%llu", ConvertToUint64());
        }
        return pchBuf;
    }

};

// various needed interfaces
class IClientFriends {
private:
    void GetFriendByIndex_(CSteamID& out, int32_t index, EFriendFlags flags) {
        Vmt::get_method<void(__thiscall*)(IClientFriends*, CSteamID&, int32_t, EFriendFlags)>(this, 10)(this, out, index, flags);
    }

    void GetClanByIndex_(CSteamID& out, int32_t index) {
        Vmt::get_method<void(__thiscall*)(IClientFriends*, CSteamID&, int32_t)>(this, 63)(this, out, index);
    }

public:

    const char* GetPersonaName() {
        return Vmt::get_method<const char*(__thiscall*)(IClientFriends*)>(this, 0)(this);
    }

    EPersonaState GetPersonaState() {
        return Vmt::get_method<EPersonaState(__thiscall*)(IClientFriends*)>(this, 4)(this);
    }

    void SetPersonaState(EPersonaState state) {
        return Vmt::get_method<void(__thiscall*)(IClientFriends*, EPersonaState)>(this, 5)(this, state);
    }

    int32_t GetFriendCount(EFriendFlags flags) {
        return Vmt::get_method < int32_t(__thiscall*)(IClientFriends*, EFriendFlags)>(this, 7)(this, flags);
    }

    bool InviteFriendToClan(CSteamID friend_to_invite, CSteamID clan) {
        return Vmt::get_method<bool(__thiscall*)(IClientFriends*, CSteamID, CSteamID)>(this, 85)(this, friend_to_invite, clan);
    }

    EPersonaState GetFriendPersonaState(CSteamID friend_wanted) {
        return Vmt::get_method<EPersonaState(__thiscall*)(IClientFriends*, CSteamID)>(this, 13)(this, friend_wanted);
    }

    const char* GetFriendPersonaName(CSteamID friend_wanted) {
        return Vmt::get_method<const char*(__thiscall*)(IClientFriends*, CSteamID)>(this, 14)(this, friend_wanted);
    }

    bool IsClanPublic(CSteamID clan_to_check) {
        return Vmt::get_method<bool(__thiscall*)(IClientFriends*, CSteamID)>(this, 70)(this, clan_to_check);
    }

    int32_t GetClanCount() {
        return Vmt::get_method<int32_t(__thiscall*)(IClientFriends*)>(this, 62)(this);
    }
    
    const char* GetClanName(CSteamID clan) {
        return Vmt::get_method<const char*(__thiscall*)(IClientFriends*, CSteamID)>(this, 64)(this, clan);
    }

    bool AcknowledgeInviteToClan(CSteamID clan_or_friend_dunno, bool accept_or_deny) {
        return Vmt::get_method<bool(__thiscall*)(IClientFriends*, CSteamID, bool)>(this, 86)(this, clan_or_friend_dunno, accept_or_deny);
    }

    __forceinline CSteamID GetClanByIndex(int32_t index) {
        CSteamID ret;

        GetClanByIndex_(ret, index);

        return ret;
    }

    __forceinline CSteamID GetFriendByIndex(int32_t index, EFriendFlags flags) {
        CSteamID ret;

        GetFriendByIndex_(ret, index, flags);

        return ret;
    }
};

class IClientEngine {
public:

    IClientFriends* GetIClientFriends(HSteamUser user, HSteamPipe pipe, const char* version) {
        return Vmt::get_method<IClientFriends*(__thiscall*)(IClientEngine*, HSteamUser, HSteamPipe, const char*)>(this, 12)(this, user, pipe, version);
   }
};