#pragma once

#include "steam_defs.hpp"

using namespace SteamInternal_Methods;
using namespace SteamAPI_Methods;

class Steam {
protected:

    // steam_appid.txt file
    std::fstream     m_appid;
    // our instance of steam_api.dll
    HMODULE          m_steam_api;

    // some exported methods
    Init_            m_init;
    CreateInterface_ m_create_interface;
    GetHSteamPipe_   m_get_steam_pipe;
    GetHSteamUser_   m_get_steam_user;

    // user and pipe (dunno if should use or not?)
    HSteamPipe       m_pipe;
    HSteamUser       m_user;

public:
    // various interfaces to play with
    IClientEngine*   m_engine;
    IClientFriends*  m_friends;

    // c/dtor
    Steam();
    ~Steam();

    // templated wrapper method to a wrapper method.. hmm..
    template<typename _T = uintptr_t>
    _T create_interface(const std::string& interface_to_get) {
        _T ret;

        ret = (_T) m_create_interface(interface_to_get.c_str());

        return ret;
    }

    // template function to grab exported functions safely
    template <typename _T = uintptr_t>
    bool get_export(const std::string& export_to_get, _T& out) {
        uintptr_t proc_addr;

        proc_addr = (uintptr_t) GetProcAddress(m_steam_api, export_to_get.c_str());
     
        if (!proc_addr) {
            dbgprint("error retrieving export %s!\n", export_to_get.c_str());
        }

        out = (_T) proc_addr;

        return proc_addr;
    }


    __forceinline HSteamPipe get_pipe() {
        return m_pipe;
    }
    __forceinline HSteamUser get_user() {
        return m_user;
    }
};